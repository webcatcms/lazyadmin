from PyQt5 import QtCore, QtGui, QtWidgets

# from lazy_admin.main_windows import *
# from lazy_admin.list_server import *
# from lazy_admin.preference import *
# from lazy_admin.login import *
# from lazy_admin.set_dirkeys import *
# from lazy_admin.formClient import *
# from lazy_admin.model import *
# from lazy_admin.dashboard import *

from main_windows import *
from list_server import *
from dashboard import *
from preference import *
from login import *
from set_dirkeys import *
from formClient import *
from model import *

import sys
import os
import socket
import ssl
import pickle
import struct
import threading
import sqlite3
import subprocess
from time import sleep

class LazyAdmin(QtWidgets.QMainWindow):

    def __init__(self, *args, **kwargs):
        super(LazyAdmin, self).__init__(*args, **kwargs)
        self.auth = False
        self.user_id = None
        self.token = None
        self.lock = threading.Lock()
        self.conn = None
        self.Login()
        if self.auth:
            self.model = Model(self.token)
            self.settings, self.account = self.model.get_settings()
            self.list_server = ListServer()
            self.dashboard = Dashboard()
            self.icon_tray = QtWidgets.QSystemTrayIcon(self)
            self.ui = Ui_MainWindow()
            self.initUI()


    def resizeEvent(self, event):
        self.AppSize = self.geometry()
        self.ui.widget.setGeometry(QtCore.QRect(0, 0, self.AppSize.width(), 45))
        self.list_server.setGeometry(QtCore.QRect(-3, 41, 230, self.AppSize.height()-37))
        self.dashboard.verticalLayoutWidget.setGeometry(QtCore.QRect(224, 50, self.AppSize.width()-230, self.AppSize.height()-55))
        self.dashboard.tabs.setGeometry(QtCore.QRect(0, 0, self.AppSize.width()-230, self.AppSize.height()))
        QtWidgets.QMainWindow.resizeEvent(self, event)

    def initUI(self):
        self.ui.setupUi(self)

        self.setWindowTitle("Lazy Admin 3.0")
        self.setWindowIcon(QtGui.QIcon("img/icon.png"))
        self.icon_tray.setIcon(QtGui.QIcon("img/icon.png"))
        self.icon_tray.show()
        self.icon_tray.setToolTip("LazyMonitor")

        self.list_server.mode = 'list'
        self.list_server.setupUi(self.model)
        self.list_server.setParent(self.ui.centralwidget)
        self.dashboard.setupUi(self.ui.centralwidget)
        self.ui.preferences.clicked.connect(self.preferences)
        self.ui.add_client.clicked.connect(self.add_client)
        self.ui.edit_server.clicked.connect(self.edit_client)
        self.ui.delete_server.clicked.connect(self.delete_client)
        self.ui.sftp.clicked.connect(self.sftp)
        self.ui.teamviewer_ft.clicked.connect(self.teamviewer_ft)
        self.ui.anydesk_ft.clicked.connect(self.anydesk_ft)
        self.list_server.Click(self.change_item_list)
        self.list_server.doubleClick(self.get_service)
        self.message = ''
        self.showMaximized()

    """LOGIN"""
    def Login(self):
        self.formDialog = QtWidgets.QDialog()
        self.formDialog.setAttribute(QtCore.Qt.WA_TranslucentBackground, True)
        self.FormLogin = FormLogin()
        self.FormLogin.setupUi(self.formDialog)
        self.formDialog.setWindowFlags(self.windowFlags() | QtCore.Qt.FramelessWindowHint)
        self.FormLogin.pushButton_singin.clicked.connect(lambda: self.Singin(self.formDialog, self.FormLogin))
        self.FormLogin.pushButton_cancel.clicked.connect(self.formDialog.close)
        self.formDialog.exec_()

    def Singin(self, dialog, form):
        settings_conn = sqlite3.connect("settings.db")
        setting_cursor = settings_conn.cursor()
        setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        settings = setting_cursor.fetchone()
        # ssl and socket
        dir_keys = settings[18]

        try:
            context = ssl.create_default_context(ssl.Purpose.SERVER_AUTH, cafile=dir_keys + "/server.crt")
            context.load_cert_chain(certfile=dir_keys + "/client.crt", keyfile=dir_keys + "/client.key")
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            sock.settimeout(20)
            self.conn = context.wrap_socket(sock, server_side=False, server_hostname="LAServer")
            if settings[4]:
                self.conn.connect((settings[5], 2021))
            else:
                self.conn.connect((settings[0], 2021))

        except Exception:

            self.get_form_dirkesys(dialog)
        data_to_server = dict()
        data_to_server.update({'login': form.lineEdit_name.text()})
        data_to_server.update({'password': form.lineEdit_password.text()})

        try:
            serialized_data = pickle.dumps(data_to_server)
            self.conn.sendall(struct.pack('>I', len(serialized_data)))
            self.conn.sendall(serialized_data)

            data_size = struct.unpack('>I', self.conn.recv(4))[0]
            received_payload = b""
            reamining_payload_size = data_size
            while reamining_payload_size != 0:
                received_payload += self.conn.recv(reamining_payload_size)
                reamining_payload_size = data_size - len(received_payload)

            if 'token' in pickle.loads(received_payload):
                self.token = pickle.loads(received_payload)['token']
                self.conn.close()
                self.auth = True
                dialog.close()


            if 'deny' in pickle.loads(received_payload):
                form.label_message.setText('Вхід відхиленно !!!')
                form.label_message.setGeometry(QtCore.QRect(120, 20, 200, 19))
                form.lineEdit_name.setText("")
                form.lineEdit_password.setText("")


            if 'auth' in pickle.loads(received_payload):
                form.label_message.setText('Логін чи пароль невірний !!!')
                form.label_message.setGeometry(QtCore.QRect(90, 20, 200, 19))
                form.lineEdit_name.setText("")
                form.lineEdit_password.setText("")

        except Exception:
            self.status_conn = False
            self.conn.close()

    def set_dirkeys(self, dialog, dir_keys):
        settings_conn = sqlite3.connect("settings.db")
        setting_cursor = settings_conn.cursor()
        setting_cursor.execute('UPDATE settings SET dir_keys=?', (dir_keys,))
        settings_conn.commit()
        dialog.close()
        self.Login()


    def get_form_dirkesys(self, dialog):
        dialog.close()
        self.formDialog = QtWidgets.QDialog()
        self.FormSetDirKeys = FormSetDirKeys()
        self.FormSetDirKeys.setupUi(self.formDialog)
        self.formDialog.setWindowFlags(self.windowFlags() | QtCore.Qt.FramelessWindowHint)
        self.FormSetDirKeys.pushButton_keys.clicked.connect(lambda: self.select_dir_keys(self.FormSetDirKeys.lineEdit_keys) )
        self.FormSetDirKeys.pushButton_save.clicked.connect(lambda: self.set_dirkeys(self.formDialog, self.FormSetDirKeys.lineEdit_keys.text()))
        self.FormSetDirKeys.pushButton_cancel.clicked.connect(self.formDialog.close)
        self.formDialog.exec_()


    """LOGIN"""

    def preferences(self):

        settings = self.model.get_settings()
        self.formDialog = QtWidgets.QDialog()
        self.FormPreference = UI_Preferences()
        self.FormPreference.setupUi(self.formDialog)
        self.FormPreference.lineEdit_host.setText(settings[0][0])
        self.FormPreference.lineEdit_login_db.setText(settings[0][2])
        password_db = self.model.cipher_suite.decrypt(bytes(settings[0][3]))
        self.FormPreference.lineEdit_password_db.setText(password_db.decode("utf-8"))
        self.FormPreference.lineEdit_dbname.setText(settings[0][9])
        self.FormPreference.lineEdit_port_db.setText(str(settings[0][1]))
        self.FormPreference.checkBox_ssh.stateChanged.connect(self.FormPreference.change_form_preference_ssh)
        self.FormPreference.checkBox_ssh.setChecked(settings[0][4])
        self.FormPreference.lineEdit_host_ssh.setText(settings[0][5])
        self.FormPreference.lineEdit_login_ssh.setText(settings[0][6])
        password_ssh = self.model.cipher_suite.decrypt(bytes(settings[0][7]))
        self.FormPreference.lineEdit_password_ssh.setText(password_ssh.decode("utf-8"))
        self.FormPreference.lineEdit_port_ssh.setText(str(settings[0][8]))
        self.FormPreference.lineEdit_port_ssh.textChanged.connect(lambda: self.FormPreference.valid_field(self.FormPreference.lineEdit_port_ssh))
        self.FormPreference.lineEdit_putty.setText(settings[0][14])
        self.FormPreference.lineEdit_winscp.setText(settings[0][15])
        self.FormPreference.lineEdit_teamviewer.setText(settings[0][16])
        self.FormPreference.lineEdit_anydesk.setText(settings[0][17])
        self.FormPreference.lineEdit_keys.setText(settings[0][18])
        self.FormPreference.pushButton_putty.clicked.connect(self.select_file_putty)
        self.FormPreference.pushButton_winscp.clicked.connect(self.select_file_winscp)
        self.FormPreference.pushButton_teamviewer.clicked.connect(self.select_file_teamviewer)
        self.FormPreference.pushButton_anydesk.clicked.connect(self.select_file_anydesk)
        self.FormPreference.pushButton_keys.clicked.connect(lambda: self.select_dir_keys(self.FormPreference.lineEdit_keys))

        if not settings[0][4]:
            self.FormPreference.lineEdit_host_ssh.setEnabled(False)
            self.FormPreference.lineEdit_login_ssh.setEnabled(False)
            self.FormPreference.lineEdit_password_ssh.setEnabled(False)
            self.FormPreference.lineEdit_port_ssh.setEnabled(False)
            self.FormPreference.check_connect_ssh.setEnabled(False)

        self.FormPreference.lineEdit_login_auth.setText(settings[1][1])
        password_auth = self.model.cipher_suite.decrypt(bytes(settings[1][2]))
        self.FormPreference.lineEdit_pass_auth.setText(password_auth.decode("utf-8"))
        self.FormPreference.lineEdit_repeat_pass_auth.setText(password_auth.decode("utf-8"))
        self.FormPreference.lineEdit_token.setText(settings[1][3])


        self.FormPreference.lineEdit_port_db.textChanged.connect(lambda: self.FormPreference.valid_field(self.FormPreference.lineEdit_port_db))
        self.FormPreference.check_connect_db.clicked.connect(lambda: self.model.checkConnect(self.FormPreference, True))
        self.FormPreference.check_connect_ssh.clicked.connect(lambda: self.model.checkConnect(self.FormPreference))
        self.FormPreference.save_preferences.clicked.connect(self.save_preferences)
        self.FormPreference.cancel_save_preferences.clicked.connect(self.formDialog.close)
        self.formDialog.exec_()

    def select_dir_keys(self, lineEdit_keys):

        dir_keys = QtWidgets.QFileDialog.getExistingDirectory()

        if dir_keys:
            lineEdit_keys.setText(dir_keys)

    def select_file_putty(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        files, _ = QtWidgets.QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "Service File (*.exe)", options=options)
        if files:
            self.FormPreference.lineEdit_putty.setText(files)

    def select_file_winscp(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        files, _ = QtWidgets.QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "Service File (*.exe)", options=options)
        if files:
            self.FormPreference.lineEdit_winscp.setText(files)

    def select_file_teamviewer(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        files, _ = QtWidgets.QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "Service File (*.exe)", options=options)
        if files:
            self.FormPreference.lineEdit_teamviewer.setText(files)

    def select_file_anydesk(self):
        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        files, _ = QtWidgets.QFileDialog.getOpenFileName(self, "QFileDialog.getOpenFileName()", "",
                                                  "Service File (*.exe)", options=options)
        if files:
            self.FormPreference.lineEdit_anydesk.setText(files)

    def save_preferences(self):
        self.model.set_settings(self.FormPreference.__dict__, self.formDialog)

    def add_client(self):
        self.user_id = None
        self.formDialog = QtWidgets.QDialog()
        self.FormClient = FormClient()
        self.FormClient.setupUi(self.formDialog)
        self.FormClient.comboBox_service.currentTextChanged.connect(self.FormClient.change_service)
        self.FormClient.pushButton_save.clicked.connect(self.save_client)
        self.FormClient.pushButton_cancel.clicked.connect(self.formDialog.close)
        self.formDialog.setWindowTitle("Додати")
        self.formDialog.setWindowIcon(QtGui.QIcon("./img/icon.png"))

        for group in self.model.get_groups():
            self.FormClient.comboBox_group.addItem(group[1], group[0])
        self.formDialog.exec_()


    def edit_client(self):
        user = self.model.get_user(self.user_id)
        self.formDialog = QtWidgets.QDialog()
        self.FormClient = FormClient()
        self.FormClient.setupUi(self.formDialog)
        self.FormClient.comboBox_service.currentTextChanged.connect(self.FormClient.change_service)
        self.FormClient.pushButton_save.clicked.connect(self.save_client)
        self.FormClient.pushButton_cancel.clicked.connect(self.formDialog.close)
        self.formDialog.setWindowTitle("Редагувати")
        self.formDialog.setWindowIcon(QtGui.QIcon("./img/icon.png"))
        self.FormClient.lineEdit_name.setText(user[1])

        for group in self.model.get_groups():
            if group[0] == user[2]:
                self.FormClient.comboBox_group.addItem(group[1], group[0])
                self.FormClient.comboBox_group.setCurrentText(group[1])
            else:
                self.FormClient.comboBox_group.addItem(group[1], group[0])

        self.FormClient.comboBox_service.setCurrentText(user[6])
        note = self.model.cipher_suite.decrypt(bytes(user[4]))
        self.FormClient.textEdit_note.setPlainText(note.decode('utf-8'))
        """RDP"""
        self.FormClient.lineEdit_rdp_host.setText(user[11])
        self.FormClient.spinBox_rdp_port.setValue(user[14])
        self.FormClient.lineEdit_rdp_login.setText(user[12])
        password = self.model.cipher_suite.decrypt(bytes(user[13]))
        self.FormClient.lineEdit_rdp_password.setText(password.decode('utf-8'))
        """TEAMVIEWER"""
        self.FormClient.spinBox_id_teamviewer.setValue(user[7])
        password = self.model.cipher_suite.decrypt(bytes(user[8]))
        self.FormClient.lineEdit_password_teamviewer.setText(password.decode('utf-8'))
        """SSH"""
        self.FormClient.lineEdit_ssh_host.setText(user[15])
        self.FormClient.spinBox_ssh_port.setValue(user[18])
        self.FormClient.lineEdit_ssh_login.setText(user[16])
        password = self.model.cipher_suite.decrypt(bytes(user[17]))
        self.FormClient.lineEdit_ssh_password.setText(password.decode('utf-8'))
        """ANYDESK"""
        self.FormClient.spinBox_id_anydesk.setValue(user[9])
        password = self.model.cipher_suite.decrypt(bytes(user[10]))
        self.FormClient.lineEdit_password_anydesk.setText(password.decode('utf-8'))
        self.formDialog.exec_()


    def delete_client(self):
        self.model.delete_client(self.user_id)
        self.list_server.get_list_items()

    def change_item_list(self, item):
        self.user_id = item.data(3, 0)
        if self.user_id:
            self.ui.edit_server.setDisabled(False)
            self.ui.delete_server.setDisabled(False)
            service = self.model.get_user(self.user_id)[6]
            if service == 'ssh':
                self.ui.sftp.setDisabled(False)
            else:
                self.ui.sftp.setDisabled(True)

            if service == 'teamviewer':
                self.ui.teamviewer_ft.setDisabled(False)
            else:
                self.ui.teamviewer_ft.setDisabled(True)

            if service == 'anydesk':
                self.ui.anydesk_ft.setDisabled(False)
            else:
                self.ui.anydesk_ft.setDisabled(True)

        else:
            self.ui.edit_server.setDisabled(True)
            self.ui.delete_server.setDisabled(True)
            self.ui.sftp.setDisabled(True)
            self.ui.teamviewer_ft.setDisabled(True)
            self.ui.anydesk_ft.setDisabled(True)



    def save_client(self):
        self.model.add_client(self.FormClient.__dict__, self.formDialog, self.user_id)
        self.list_server.get_list_items()


    def get_service(self, item):
        id = item.data(3, 0)
        if id:
            user = self.model.get_user(id)
            self.run_service(user)

    def run_service(self, args):
        app = getattr(self, args[6])
        app(args)

    def rdp(self, args):
        password = self.model.cipher_suite.decrypt(bytes(args[13]))
        cmdkey = r"cmdkey /generic:{} /user:{} /pass:{}".format(args[11], args[12], password.decode("utf-8"))
        subprocess.Popen(cmdkey)
        command = r"mstsc /v {}:{}".format(args[11], args[14])
        subprocess.Popen(command)
        sleep(3)
        cdmkey_delete = r"cmdkey /delete:{}".format(args[11])
        subprocess.Popen(cdmkey_delete)

    def ssh(self, args):
        host = args[15]
        port = args[18]
        username = args[16]
        password = self.model.cipher_suite.decrypt(bytes(args[17])).decode('utf-8')
        command = "command.txt"
        subprocess.Popen(r"{} {}@{} {} -pw {} -m {} -t".format(self.settings[14], username, host, port, password, command))


    def sftp(self):
        if self.user_id:
            args = self.model.get_user(self.user_id)
        host = args[15]
        port = args[18]
        username = args[16]
        password = self.model.cipher_suite.decrypt(bytes(args[17])).decode('utf-8')
        subprocess.Popen(r"{} sftp://{}:{}@{}:{}".format(self.settings[15], username, password, host, port))


    def teamviewer(self, args):
        password = self.model.cipher_suite.decrypt(bytes(args[8]))
        command = r"{} -i {} --Password {}".format(self.settings[16], str(args[7]), password.decode("utf-8"))
        subprocess.Popen(command)

    def teamviewer_ft(self):
        if self.user_id:
            args = self.model.get_user(self.user_id)
        password = self.model.cipher_suite.decrypt(bytes(args[8]))
        command = r"{} -i {} --Password {} --mode fileTransfer".format(self.settings[16], str(args[7]), password.decode("utf-8"))
        subprocess.Popen(command)

    def anydesk(self, args):
        password = self.model.cipher_suite.decrypt(bytes(args[10]))
        os.chdir(self.settings[17].replace('AnyDesk.exe', ''))
        command = r"echo {} | AnyDesk.exe {} --with-password".format(password.decode('utf-8'), args[9])
        os.popen(command)


    def anydesk_ft(self):
        if self.user_id:
            args = self.model.get_user(self.user_id)
        password = self.model.cipher_suite.decrypt(bytes(args[10]))
        os.chdir(self.settings[17].replace('AnyDesk.exe', ''))
        command = r"echo {} | AnyDesk.exe {} --with-password --file-transfer".format(password.decode('utf-8'), args[9])
        os.popen(command)

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    styleSheet = open("style.css", "r").read()
    app.setStyleSheet(styleSheet)
    ex = LazyAdmin()
    if ex.auth:
        sys.exit(app.exec_())
