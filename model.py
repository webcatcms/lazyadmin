import psycopg2
import sys
from PyQt5 import QtGui, QtWidgets
from sshtunnel import *
import sqlite3
from cryptography.fernet import Fernet
class Model(object):
    _instance = None

    def __new__(cls, token=None):
        if cls._instance is None:
           cls._instance = super(Model, cls).__new__(cls)
        return cls._instance

    def __init__(self, token=None):
        self.settings_conn = sqlite3.connect("settings.db")
        self.setting_cursor = self.settings_conn.cursor()
        self.setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        self.settings = self.setting_cursor.fetchone()
        self.cipher_suite = Fernet(token)
        password_db = self.cipher_suite.decrypt(bytes(self.settings[3]))
        password_db = password_db.decode("utf-8")
        password_ssh = self.cipher_suite.decrypt(bytes(self.settings[7]))
        password_ssh = password_ssh.decode("utf-8")
        if self.settings[4]:
            self.server = open_tunnel(
                    (self.settings[5], self.settings[8]),
                    ssh_username=self.settings[6],
                    ssh_password=password_ssh,
                    remote_bind_address=(self.settings[0], self.settings[1]))
            self.server.start()

            try:
                self.conn = psycopg2.connect(
                    database=self.settings[9],
                    user=self.settings[2],
                    password=password_db,
                    host=self.settings[0],
                    port=self.server.local_bind_port,
                    sslmode='require')
                self.cursor = self.conn.cursor()
            except Exception:
                self.message_info("Не можливо підключитись до БД!!!")
        else:
            try:
                self.conn = psycopg2.connect(
                    database=self.settings[9],
                    user=self.settings[2],
                    password=password_db,
                    host=self.settings[0],
                    port=self.settings[1])
                self.cursor = self.conn.cursor()
            except Exception:
                self.message_info("Не можливо підключитись до БД!!!")

    def __del__(self):
        if self.cursor:
            self.cursor.close()
        self.server.close()

    def message_info(self, str_text):
        msg = QtWidgets.QMessageBox()
        msg.setWindowIcon(QtGui.QIcon("./img/icon.png"))
        msg.setIcon(QtWidgets.QMessageBox.Warning)
        msg.setWindowTitle("Повідомлення")
        msg.setText(str_text)
        msg.addButton('Закрити', QtWidgets.QMessageBox.RejectRole)
        msg.exec()

    def get_settings(self):
        self.setting_cursor.execute('SELECT * FROM settings LIMIT 1')
        self.cursor.execute("SELECT * FROM account WHERE id = 1")
        return (self.setting_cursor.fetchone(), self.cursor.fetchone())

    def set_settings(self, data, formDialog):

        args = (
            data['lineEdit_host'].text(),
            int(data['lineEdit_port_db'].text()),
            data['lineEdit_login_db'].text(),
            self.cipher_suite.encrypt(bytes(str(data['lineEdit_password_db'].text().replace(" ", "")), "utf-8")),
            int(data['checkBox_ssh'].isChecked()),
            data['lineEdit_host_ssh'].text(),
            data['lineEdit_login_ssh'].text(),
            self.cipher_suite.encrypt(bytes(str(data['lineEdit_password_ssh'].text().replace(" ", "")), "utf-8")),
            int(data['lineEdit_port_ssh'].text()),
            data['lineEdit_dbname'].text(),
            data['lineEdit_keys'].text(),
            data['lineEdit_putty'].text(),
            data['lineEdit_winscp'].text(),
            data['lineEdit_teamviewer'].text(),
            data['lineEdit_anydesk'].text(),
                )

        sql = ''' UPDATE settings SET 
            host=?,
            port=?,
            login=?,
            password=?,
            ssh=?,
            ssh_host=?,
            ssh_login=?,
            ssh_password=?,
            ssh_port=?,
            dbname=?,
            dir_keys=?,
            putty_dir=?, 
            winscp_dir=?,
            teamviewer_dir=?,
            anydesk_dir=?         
            '''

        login = data['lineEdit_login_auth'].text()
        password = self.cipher_suite.encrypt(bytes(str(data['lineEdit_pass_auth'].text().replace(" ", "")), "utf-8"))
        token = data['lineEdit_token'].text()

        try:
            self.cursor.execute("UPDATE account SET login=%s, password=%s, token=%s WHERE id=1", (login, password, token))
            self.setting_cursor.execute(sql, args)
            self.conn.commit()
            self.settings_conn.commit()
            formDialog.close()

        except Exception:
            type, value, traceback = sys.exc_info()
            self.message_info(value.args[0])


    def is_server(self, server_id):
        try:
            self.cursor.execute('SELECT * FROM servers WHERE id = %s', (server_id,))
            server = self.cursor.fetchone()
            return server
        except IndexError:
            return False

    def add_client(self, data, formDialog, user_id=None):
        if user_id:
            self.cursor.execute("SELECT * FROM users WHERE id = %s", (user_id,))
            user = self.cursor.fetchone()
            client_name = data['lineEdit_name'].text()
            group_id = data['comboBox_group'].currentData()
            service = data['comboBox_service'].currentText()
            self.cursor.execute("UPDATE service SET name = %s  WHERE id = %s", (service, user[3],))
            note = self.cipher_suite.encrypt(bytes(str(data['textEdit_note'].toPlainText().replace(" ", "")), "utf-8"))
            self.cursor.execute("UPDATE users SET name = %s, group_id = %s, service_id = %s, note = %s WHERE id = %s", (client_name, group_id, user[3], note, user[0]))
            teamviewer_id = data['spinBox_id_teamviewer'].value()
            teamviewer_password = self.cipher_suite.encrypt( bytes(str(data['lineEdit_password_teamviewer'].text().replace(" ", "")), "utf-8"))
            anydesk_id = data['spinBox_id_anydesk'].value()
            anydesk_password = self.cipher_suite.encrypt(bytes(str(data['lineEdit_password_anydesk'].text().replace(" ", "")), "utf-8"))
            rdp_host = data['lineEdit_rdp_host'].text()
            rdp_login = data['lineEdit_rdp_login'].text()
            rdp_password = self.cipher_suite.encrypt(bytes(str(data['lineEdit_rdp_password'].text().replace(" ", "")), "utf-8"))
            rdp_port = data['spinBox_rdp_port'].value()
            ssh_host = data['lineEdit_ssh_host'].text()
            ssh_login = data['lineEdit_ssh_login'].text()
            ssh_password = self.cipher_suite.encrypt(bytes(str(data['lineEdit_ssh_password'].text().replace(" ", "")), "utf-8"))
            ssh_port = data['spinBox_ssh_port'].value()
            self.cursor.execute("UPDATE services SET "
                                "teamviewer_id = %s,"
                                "teamviewer_password = %s,"
                                "anydesk_id = %s,"
                                "anydesk_password = %s,"
                                "rdp_host = %s,"
                                "rdp_login = %s,"
                                "rdp_password = %s,"
                                "rdp_port = %s ,"
                                "ssh_host = %s,"
                                "ssh_login = %s,"
                                "ssh_password = %s,"
                                "ssh_port = %s WHERE service_id = %s",
                                (teamviewer_id,
                                 teamviewer_password,
                                 anydesk_id,
                                 anydesk_password,
                                 rdp_host,
                                 rdp_login,
                                 rdp_password,
                                 rdp_port,
                                 ssh_host,
                                 ssh_login,
                                 ssh_password,
                                 ssh_port,
                                 user[3]))
            self.conn.commit()
        else:
            service = data['comboBox_service'].currentText()
            client = data['lineEdit_name'].text()
            note = self.cipher_suite.encrypt(bytes(str(data['textEdit_note'].toPlainText().replace(" ", "")), "utf-8"))
            self.cursor.execute("INSERT INTO service (name) VALUES (%s) RETURNING id", (service,))
            service_id = self.cursor.fetchone()[0]
            self.cursor.execute("SELECT id FROM groups WHERE name = %s ", (data['comboBox_group'].currentText(),))
            group_id = self.cursor.fetchone()[0]
            self.cursor.execute("INSERT INTO users (name, group_id, service_id, note) VALUES (%s,%s,%s,%s)", (client, group_id, service_id, note,))

            teamviewer_id = data['spinBox_id_teamviewer'].value()
            teamviewer_password = self.cipher_suite.encrypt(bytes(str(data['lineEdit_password_teamviewer'].text().replace(" ", "")), "utf-8"))
            anydesk_id = data['spinBox_id_anydesk'].value()
            anydesk_password = self.cipher_suite.encrypt(bytes(str(data['lineEdit_password_anydesk'].text().replace(" ", "")), "utf-8"))
            rdp_host = data['lineEdit_rdp_host'].text()
            rdp_login = data['lineEdit_rdp_login'].text()
            rdp_password = self.cipher_suite.encrypt(bytes(str(data['lineEdit_rdp_password'].text().replace(" ", "")), "utf-8"))
            rdp_port = data['spinBox_rdp_port'].value()
            ssh_host = data['lineEdit_ssh_host'].text()
            ssh_login = data['lineEdit_ssh_login'].text()
            ssh_password = self.cipher_suite.encrypt(bytes(str(data['lineEdit_ssh_password'].text().replace(" ", "")), "utf-8"))
            ssh_port = data['spinBox_ssh_port'].value()
            service_id = service_id

            self.cursor.execute("INSERT INTO services ("
                                "teamviewer_id, "
                                "teamviewer_password,"
                                "anydesk_id,"
                                "anydesk_password,"
                                "rdp_host,"
                                "rdp_login,"
                                "rdp_password,"
                                "rdp_port,"
                                "ssh_host,"
                                "ssh_login,"
                                "ssh_password,"
                                "ssh_port,"
                                "service_id) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)",
                                (teamviewer_id,
                                 teamviewer_password,
                                 anydesk_id,
                                 anydesk_password,
                                 rdp_host,
                                 rdp_login,
                                 rdp_password,
                                 rdp_port,
                                 ssh_host,
                                 ssh_login,
                                 ssh_password,
                                 ssh_port,
                                 service_id))
            self.conn.commit()

        formDialog.close()


    def delete_client(self, id):
        if id:
            self.cursor.execute("DELETE FROM service WHERE id = (SELECT service_id FROM users WHERE id = %s)", (id,))
        self.conn.commit()
        id = None

    def update_server(self, *args):
        self.cursor.execute("UPDATE servers SET name_server = %s, name_id = %s, port = %s, status = %s, host = %s, login_rdp = %s, password_rdp = %s, port_rdp = %s, id_teamviewer = %s, password_teamviewer = %s, login_ssh = %s, password_ssh = %s, port_ssh = %s, group_id = %s, enable_rdp = %s, enable_teamviewer = %s, enable_ssh = %s WHERE id = %s ", args)
        self.conn.commit()

    def get_servers(self):
        self.cursor.execute("SELECT u.*, g.name FROM users u LEFT JOIN groups g ON g.id = u.group_id ORDER BY u.name ASC")
        servers = self.cursor.fetchall()
        return servers

    def get_user(self, id):
        self.cursor.execute("SELECT u.*, s.*, ss.*  FROM users u LEFT JOIN service s ON s.id = u.service_id LEFT JOIN services ss ON s.id = ss.service_id WHERE u.id = %s", (id,))
        user = self.cursor.fetchone()
        return user

    def get_groups(self):
        self.cursor.execute("SELECT * FROM groups")
        groups = self.cursor.fetchall()
        return groups

    def checkConnect(self, form_db, db=False):
        if db:
            msg = QtWidgets.QMessageBox()
            msg.setWindowIcon(QtGui.QIcon("./img/icon.png"))
            msg.setWindowTitle("Повідомлення")

            try:
                conn = psycopg2.connect(
                    database=form_db.lineEdit_dbname.text(),
                    user=form_db.lineEdit_login_db.text(),
                    password=form_db.lineEdit_password_db.text(),
                    host=form_db.lineEdit_host.text(),
                    port=form_db.lineEdit_port_db.text())

                self.cursor = conn.cursor()
                msg.setIcon(QtWidgets.QMessageBox.Information)
                msg.setText("Підключення до БД Успішне !!!")
            except psycopg2.OperationalError:
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText("Не можливо підключитись до БД !!!")

            msg.addButton('Закрити', QtWidgets.QMessageBox.RejectRole)
            msg.exec()
        else:
            msg = QtWidgets.QMessageBox()
            msg.setWindowIcon(QtGui.QIcon("./img/icon.png"))
            msg.setWindowTitle("Повідомлення")

            try:
                self.server = open_tunnel(
                    (form_db.lineEdit_host_ssh.text(), int(form_db.lineEdit_port_ssh.text())),
                    ssh_username=form_db.lineEdit_login_ssh.text(),
                    ssh_password=form_db.lineEdit_password_ssh.text(),
                    remote_bind_address=(form_db.lineEdit_host.text(), int(form_db.lineEdit_port_db.text())))
                self.server.start()
                self.server.stop()
                msg.setIcon(QtWidgets.QMessageBox.Information)
                msg.setText("Підключення Успішне !!!")
            except Exception:
                type, value, traceback = sys.exc_info()
                msg.setIcon(QtWidgets.QMessageBox.Critical)
                msg.setText(value.args[0])

            msg.addButton('Закрити', QtWidgets.QMessageBox.RejectRole)
            msg.exec()

