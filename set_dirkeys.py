# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets


class FormSetDirKeys(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("DialogSetDirKeys")
        Dialog.resize(350, 150)

        """ Form general """
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(5, 5, 340, 140))
        self.groupBox.setObjectName("groupBox_set_dirkeys")
        self.groupBox.setTitle(' Ключі не знайденно!!! ')
        self.label_keys = QtWidgets.QLabel(Dialog)
        self.label_keys.setGeometry(QtCore.QRect(20, 23, 35, 20))
        self.label_keys.setObjectName("label_keys")
        self.label_keys.setText("Ключі:")
        self.lineEdit_keys = QtWidgets.QLineEdit(self.groupBox)
        self.lineEdit_keys.setGeometry(QtCore.QRect(60, 20, 200, 20))
        self.lineEdit_keys.setObjectName("lineEdit_keys")

        self.pushButton_keys = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_keys.setGeometry(QtCore.QRect(270, 20, 39, 21))
        self.pushButton_keys.setObjectName("pushButton_keys")
        self.pushButton_keys.setText('...')


        self.pushButton_cancel = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_cancel.setGeometry(QtCore.QRect(235, 100, 75, 23))
        self.pushButton_cancel.setObjectName("pushButton_cancel")
        self.pushButton_cancel.setText("Відміна")
        self.pushButton_save = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_save.setDefault(True)
        self.pushButton_save.setGeometry(QtCore.QRect(156, 100, 75, 23))
        self.pushButton_save.setObjectName("pushButton_save")
        self.pushButton_save.setText("Зберегти")
        """ Form general """

        QtCore.QMetaObject.connectSlotsByName(Dialog)
