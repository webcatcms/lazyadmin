# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets


class FormLogin(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("DialogLogin")
        Dialog.resize(362, 200)
        self.visibleIcon = QtGui.QIcon("img/eye_visible.svg")
        self.hiddenIcon = QtGui.QIcon("img/eye_hidden.svg")
        self.password_shown = False
        """ Form general """
        self.groupBox = QtWidgets.QGroupBox(Dialog)
        self.groupBox.setGeometry(QtCore.QRect(10, 10, 341, 180))
        #self.groupBox.setTitle("Авторизуйтесь")
        self.groupBox.setObjectName("groupBoxLogin")
        self.label_message = QtWidgets.QLabel(self.groupBox)
        self.label_message.setGeometry(QtCore.QRect(120, 20, 200, 19))
        self.label_message.setObjectName("label_message")
        self.label_name = QtWidgets.QLabel(self.groupBox)
        self.label_name.setGeometry(QtCore.QRect(50, 50, 34, 16))
        self.label_name.setObjectName("label_login")
        self.label_name.setText("Логін:")
        self.lineEdit_name = QtWidgets.QLineEdit(self.groupBox)
        self.lineEdit_name.setGeometry(QtCore.QRect(90, 50, 200, 19))
        self.lineEdit_name.setObjectName("lineEdit_login")

        self.label_password = QtWidgets.QLabel(self.groupBox)
        self.label_password.setGeometry(QtCore.QRect(39, 80, 45, 16))
        self.label_password.setObjectName("label_password")
        self.label_password.setText("Пароль:")
        self.lineEdit_password = QtWidgets.QLineEdit(self.groupBox)
        self.lineEdit_password.setGeometry(QtCore.QRect(90, 80, 200, 19))
        self.lineEdit_password.setObjectName("lineEdit_password")
        self.lineEdit_password.setEchoMode(QtWidgets.QLineEdit.Password)
        togglepassword = self.lineEdit_password.addAction(self.hiddenIcon, QtWidgets.QLineEdit.TrailingPosition)
        togglepassword.triggered.connect(lambda: self.on_toggle_password_Action(self.lineEdit_password, togglepassword))





        self.pushButton_cancel = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_cancel.setGeometry(QtCore.QRect(200, 120, 75, 23))
        self.pushButton_cancel.setObjectName("pushButton_cancel")
        self.pushButton_cancel.setText("Відміна")
        self.pushButton_singin = QtWidgets.QPushButton(self.groupBox)
        self.pushButton_singin.setDefault(True)
        self.pushButton_singin.setGeometry(QtCore.QRect(100, 120, 75, 23))
        self.pushButton_singin.setObjectName("pushButton_singin")
        self.pushButton_singin.setText("Увійти")
        """ Form general """



        QtCore.QMetaObject.connectSlotsByName(Dialog)




    def on_toggle_password_Action(self, lineEditPass, togglepasswordAction):
        if not self.password_shown:
            lineEditPass.setEchoMode(QtWidgets.QLineEdit.Normal)
            self.password_shown = True
            togglepasswordAction.setIcon(self.visibleIcon)
        else:
            lineEditPass.setEchoMode(QtWidgets.QLineEdit.Password)
            self.password_shown = False
            togglepasswordAction.setIcon(self.hiddenIcon)