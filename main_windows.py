# -*- coding: utf-8 -*-

from PyQt5 import QtCore, QtGui, QtWidgets

# from lazy_admin.helpers.pushButtonAnimation import *


from helpers.pushButtonAnimation import *


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.setMinimumSize(1280, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")

        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setObjectName("widget")

        self.horizontalLayoutWidget = QtWidgets.QWidget(self.widget)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(2, 2, 300, 42))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")



        self.add_client = PushButton(self.horizontalLayoutWidget)
        self.add_client.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("img/addServer.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.add_client.setIcon(icon)
        self.add_client.setIconSize(QtCore.QSize(32, 32))
        self.add_client.setObjectName("add_client")
        self.horizontalLayout.addWidget(self.add_client)


        self.edit_server = PushButton(self.horizontalLayoutWidget)
        self.add_client.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("img/editServer.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.edit_server.setIcon(icon1)
        self.edit_server.setIconSize(QtCore.QSize(32, 32))
        self.edit_server.setObjectName("edit_server")
        self.edit_server.setDisabled(True)
        self.horizontalLayout.addWidget(self.edit_server)


        self.delete_server = PushButton(self.horizontalLayoutWidget)
        self.delete_server.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.delete_server.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("img/deleteServer.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.delete_server.setIcon(icon2)
        self.delete_server.setIconSize(QtCore.QSize(32, 32))
        self.delete_server.setObjectName("delete_server")
        self.delete_server.setDisabled(True)
        self.horizontalLayout.addWidget(self.delete_server)

        self.sftp = PushButton(self.horizontalLayoutWidget)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("img/sftp.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.sftp.setIcon(icon3)
        self.sftp.setIconSize(QtCore.QSize(32, 32))
        self.sftp.setObjectName("sftp")
        self.sftp.setDisabled(True)
        self.horizontalLayout.addWidget(self.sftp)

        self.teamviewer_ft = PushButton(self.horizontalLayoutWidget)
        icon4 = QtGui.QIcon()
        icon4.addPixmap(QtGui.QPixmap("img/TeamViewer.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.teamviewer_ft.setIcon(icon4)
        self.teamviewer_ft.setIconSize(QtCore.QSize(32, 32))
        self.teamviewer_ft.setObjectName("teamviewer_ft")
        self.teamviewer_ft.setDisabled(True)
        self.horizontalLayout.addWidget(self.teamviewer_ft)

        self.anydesk_ft = PushButton(self.horizontalLayoutWidget)
        icon5 = QtGui.QIcon()
        icon5.addPixmap(QtGui.QPixmap("img/anydesk.svg"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.anydesk_ft.setIcon(icon5)
        self.anydesk_ft.setIconSize(QtCore.QSize(32, 32))
        self.anydesk_ft.setObjectName("anydesk_ft")
        self.anydesk_ft.setDisabled(True)
        self.horizontalLayout.addWidget(self.anydesk_ft)

        self.preferences = PushButton(self.horizontalLayoutWidget)
        self.preferences.setSizePolicy(QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        icon3 = QtGui.QIcon()
        icon3.addPixmap(QtGui.QPixmap("img/preferences.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.preferences.setIcon(icon3)
        self.preferences.setIconSize(QtCore.QSize(32, 32))
        self.preferences.setObjectName("preferences")
        self.horizontalLayout.addWidget(self.preferences)
        MainWindow.setCentralWidget(self.centralwidget)


        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.add_client.setToolTip(_translate("MainWindow", "<html><head/><body><p>Додати ПК</p></body></html>"))
        self.edit_server.setToolTip(_translate("MainWindow", "<html><head/><body><p>Редагувати ПК</p></body></html>"))
        self.delete_server.setToolTip(_translate("MainWindow", "<html><head/><body><p>Видалити ПК</p></body></html>"))
        self.sftp.setToolTip(_translate("MainWindow", "<html><head/><body><p>Передача файлів</p></body></html>"))
        self.teamviewer_ft.setToolTip(_translate("MainWindow", "<html><head/><body><p>Передача файлів</p></body></html>"))
        self.anydesk_ft.setToolTip(_translate("MainWindow", "<html><head/><body><p>Передача файлів</p></body></html>"))
        self.preferences.setToolTip(_translate("MainWindow", "<html><head/><body><p>Налаштування</p></body></html>"))
