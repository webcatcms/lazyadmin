from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtWidgets import *
from PyQt5.QtWebEngineWidgets import *
from PyQt5.QtCore import *

class Dashboard(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()



    def setupUi(self, widget):
        self.verticalLayoutWidget = QtWidgets.QWidget(widget)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 781, 521))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.tabs = QTabWidget(self.verticalLayoutWidget)
        self.tabs.setDocumentMode(True)
        self.tabs.setTabsClosable(False)
        self.tabs.tabCloseRequested.connect(self.close_current_tab)
        args = [
            [
                "https://site1.com",
                "site1",
                True,
            ],
            [
                "https://sites2.com",
                "site2",
                False,
            ],
            [
                "https://site3.com",
                "site3",
                False,
            ],
        ]
        self.add_tabs(args)

    def add_tabs(self, args):

        for tab in args:
            browser = QWebEngineView()

            browser.setUrl(QUrl(tab[0]))

            i = self.tabs.addTab(browser, tab[1])
            if tab[2]:
                self.tabs.setCurrentIndex(i)



    def close_current_tab(self, i):

        # if there is only one tab
        if self.tabs.count() < 2:
            # do nothing
            return

        # else remove the tab
        self.tabs.removeTab(i)